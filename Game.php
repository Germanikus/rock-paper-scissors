<?php

class Game
{

    const ROLE1 = 1;
    const ROLE2 = 2;
    const ROLE3 = 3;

    private $roles = [
        self::ROLE1 => 'Rock',
        self::ROLE2 => 'Paper',
        self::ROLE3 => 'Scissors',
    ];

    public $rounds;

    private $players = 2;

    private $gameLog = [
        'player_one_score' => 0,
        'player_two_score' => 0
    ];

    /**
     * Get game roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set game roles
     *
     * @param  array $roles
     * @return void
     */
    public function setRoles(array $roles)
    {
        if (count($roles) != count($this->roles)) {
            throw new \Exception("Array should have ". count($this->roles) . " elemnets" , 1);
        }
        $this->roles = array_combine(array_flip($this->roles), $roles);
    }

    /**
     * Set rounds
     *
     * @param  integer $rounds
     * @return void
     */
    public function playRounds($rounds = 3)
    {
        $this->rounds = $rounds;
    }

    /**
     * Execute game
     *
     * @return void
     */
    public function play()
    {
        for ($round = 1; $round <= $this->rounds; $round++) {
            $this->gameLog[] = '<strong> Round ' . $round . '</strong> <hr>';

            $roundRoles = $this->drawHands();

            if (isset($roundRoles)) {
                $this->evaluateRound($roundRoles);
            }
        }

        $this->printOutput();
    }

    /**
     * Checks which player is winner
     *
     * @return string
     */
    public function winner()
    {
        if ($this->gameLog['player_one_score'] > $this->gameLog['player_two_score']) {
            echo '<h1> Winner is Player One </h1>';
        } else {
            echo '<h1> Winner is Player Two </h1>';
        }
    }

    /**
     * Prints game output
     *
     * @return void
     */
    public function printOutput()
    {
        foreach ($this->gameLog as $key => $output) {
            if ($key === 'player_one_score' || $key === 'player_two_score') {
                continue;
            }
            echo  $output . '<br>';
        }
    }

    /**
     * Draws a hand for each player
     *
     * @return array
     */
    public function drawHands()
    {
        $roundRoles = [];
        $i = 0;

        while ($i < $this->players) {
            $newRole = array_rand($this->getRoles());
            $i++;
            $roundRoles[] = $newRole;

            if (count($roundRoles) == $this->players && $roundRoles[0] == $roundRoles[1]) {
                $this->gameLog[] = 'Draw Round';
                unset($roundRoles);
                $i = 0;
                continue;
            }
        }
        return $roundRoles;
    }

    /**
     * Evaluate which player the round winner
     *
     * @param  array $roundRoles
     * @return void
     */
    public function evaluateRound($roundRoles)
    {
        $playerOne = $roundRoles[0];
        $playerTwo = $roundRoles[1];

        $this->gameLog[] = 'Player One Hand: <strong>'. $this->getRoles()[$playerOne] . '</strong> - Player Two Hand: <strong>' . $this->getRoles()[$playerTwo] . '</strong>';

        // player one win conditions
        if ($playerOne == self::ROLE1 && $playerTwo == self::ROLE3) {
            $this->gameLog['player_one_score'] += 1;
        }

        if ($playerOne == self::ROLE3 && $playerTwo == self::ROLE2) {
            $this->gameLog['player_one_score'] += 1;
        }

        if ($playerOne == self::ROLE2 && $playerTwo == self::ROLE1) {
            $this->gameLog['player_one_score'] += 1;
        }

        // player two win conditions
        if ($playerTwo == self::ROLE1 && $playerOne == self::ROLE3) {
            $this->gameLog['player_two_score'] += 1;
        }

        if ($playerTwo == self::ROLE3 && $playerOne == self::ROLE2) {
            $this->gameLog['player_two_score'] += 1;
        }

        if ($playerTwo == self::ROLE2 && $playerOne == self::ROLE1) {
            $this->gameLog['player_two_score'] +=1;
        }

        $this->gameLog[] = '<br> Round Score: <br> Player One: '. $this->gameLog['player_one_score'] . ' - Player Two: '. $this->gameLog['player_two_score'] .'<br>';
    }

}