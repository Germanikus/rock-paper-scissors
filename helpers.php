<?php

if ( ! function_exists('dd')) {

    function dd(...$args)
    {
        foreach ($args as $var) {
            echo '<pre>' . var_dump($var) . '</pre>';
        }
        die(1);
    }
}
