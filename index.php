<?php

require_once('helpers.php');
require_once('Game.php');

$game = new Game;

$game->playRounds();
$game->play();
$game->winner();
